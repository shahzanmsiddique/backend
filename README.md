# MamunTask

This project was built with NodeJS.The app serves as a websocket server for front end app to communicate.  
It connects to a MongoDB , creates a db named `mamun` and a collection called `credit-cards` then proceeds to save data to it on client request.

## Install dependencies
After cloning into folder , run `npm install` to install dependencies.

## Run 
Add MongoDB connection string to `MONGO_URL` property in .env file for database connection.
Run `npm start` to run the node application.