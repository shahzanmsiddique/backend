// Node.js WebSocket server script
const http = require("http");
const WebSocketServer = require("websocket").server;
const { MongoClient } = require("mongodb");
require("dotenv").config();
const server = http.createServer();
server.listen(9898);
const wsServer = new WebSocketServer({
  httpServer: server,
});

function convert(usd) {
  return 0.39 * usd;
}

wsServer.on("request", function (request) {
  console.log("connected" + request.origin);

  const connection = request.accept();

  connection.on("message", async function (message) {
    const data = JSON.parse(message.utf8Data);
    console.log("Received Message:", message);
    const c = data.cardNo.charAt(data.cardNo.length - 1);

    let omr = data.currency === "OMR" ? data.amount : convert(data.amount);
    let resp;
    if (c === "5") {
      resp = {
        success: false,
        amount: omr,
      };
    } else {
      resp = {
        success: true,
        amount: omr,
      };
    }
    if (data.isToSave) {
      const row = {
        name: data.name,
        cardNo: data.cardNo,
        cvv: data.cvv,
        expiry: data.expiry,
      };
      const uri = process.env.MONGO_URL;

      const client = new MongoClient(uri);
      try {
        // Connect to the MongoDB cluster
        await client.connect(async (err, db) => {
          if (!err) {
            console.log("connection created");
            const mamunDB = client.db("mamun");
            mamunDB
              .listCollections({
                name: "credit-cards",
              })
              .next(function (err, collinfo) {
                if (!collinfo) {
                  mamunDB.createCollection("credit-cards");
                }
              });
            const col = mamunDB.collection("credit-cards");

            const result = await col.insertOne(row);
            console.log(result);
            await client.close();
          }
        });
      } catch (e) {
        console.error(e);
      }
    }
    connection.send(JSON.stringify(resp));
  });

  connection.on("close", function (reasonCode, description) {
    console.log("Client has disconnected.");
    console.log(reasonCode);
  });
});
